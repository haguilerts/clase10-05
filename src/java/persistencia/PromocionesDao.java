
package persistencia;

import entidades.Promocion;
import java.util.ArrayList;

public class PromocionesDao {
    public static ArrayList obtener(){
       String promo ="cabaña fin de semana descuento 50% de descuento ";
         ArrayList miListado =new ArrayList();
         
         Promocion miPromo1= new Promocion("cabaña", "50% de descuento");
         Promocion miPromo2= new Promocion("Empanadas","30% de descuento");
         Promocion miPromo3= new Promocion("Donas","3x2");
         Promocion miPromo4= new Promocion("mc","2x1");
          
         miListado.add(miPromo1);
         miListado.add(miPromo2);
         miListado.add(miPromo3);
         miListado.add(miPromo4);
         
         return miListado;
         
    }
    
}
