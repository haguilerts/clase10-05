package entidades;
public class Promocion {
    String nombres;
    String descripcion;

    public Promocion(String nombres, String descripcion) {
        setNombres(nombres);
        setDescripcion(descripcion);
    }
        public String getNombres() {
            return nombres;}
        public void setNombres(String nombres) {
            this.nombres = nombres;}
        public String getDescripcion() {
            return descripcion;}
        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;}

    @Override
    public String toString() {
        return "Promocion{" + "nombres="+nombres  +  ", descripcion="+descripcion + '}';
    }
     
    
}
